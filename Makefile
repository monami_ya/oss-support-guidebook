
export PATH += :/Applications/UpTeX.app/teTeX/bin

all: clean
	echo $(PATH)
	review-pdfmaker book.yaml
	review-epubmaker book.yaml

clean:
	-rm -f oss-support-guidebook.pdf
	-rm -f oss-support-guidebook.epub
	-rm -fr oss-support-guidebook-pdf

archive:
	zip tatsu-zine_copt.zip *.pdf *.re images/*
